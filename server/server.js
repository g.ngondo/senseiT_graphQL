var express = require('express');
var graphqlHTTP = require('express-graphql');
var schema = require("./domains/schema/schema");
var { createServer } = require('http');

const PORT = 4000;

var app = express();
app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true
}));

app.use('/', (req, res) => {
    res.json('Go to /graphql to test your queries and mutations!');
});
var server = createServer(app);


server.listen(PORT, () => {
    console.log('Running a GraphQL API server at localhost:4000/graphql');
});