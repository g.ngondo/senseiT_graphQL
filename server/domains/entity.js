var uuidv4 = require('uuid/v4');


class Entity {
    constructor(type, name) {
        this.id = uuidv4();
        this.type = type;
        this.name = name;
        this.relatedEntities = [];
        this.level = 0;
        this.meta = {};
        this.url = "";
        this.createdDate = new Date();
    }
}

module.exports = Entity;