module.exports = {
    findOneBy: (id, array) => {
        return array.find((entity) => {
            return entity.id == id;
        });
    },
    updateOneBy: (id, array, newobj) => {
        var old = array.find((obj) => (obj.id == id));
        var index = array.indexOf(old);
        array.splice(index, 1, newobj);
    },
    removeOneby: (id, array) => {
        var old = array.find((obj) => (obj.id == id));
        var index = array.indexOf(old);
        array.splice(index, 1);
    },
}