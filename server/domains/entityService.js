var Entity = require("./entity");

var config = require("../architecture/config");
var r = require('../architecture/dash');
var bluebird = require('bluebird');


module.exports = {
    initDb: bluebird.coroutine(function*() {
        try {
            yield r.dbCreate(config.db);
            console.log("Database `dashex` created");

            yield r.db(config.db).tableCreate(config.table);
            console.log("Table `todo` created");

            yield r.db(config.db).table(config.table).indexCreate("createdDate");
            console.log("Index `createdDate` created.");

        } catch (e) {
            console.log(e.message);
            console.log(e);
        }
    }),
    getEntities: bluebird.coroutine(function*() {
        return yield r.db(config.db).table(config.table);
    }),
    getEntity: bluebird.coroutine(function*(id) {
        return yield r.table(config.db).get(config.table);
    }),
    createEntity: bluebird.coroutine(function*(args) {
        var entity = new Entity(args.type, args.name);

        if (args.parent !== null && args.parent !== undefined) {
            var parent = yield r.db(config.db).table(config.table).get(args.parent);

            if (parent === null || parent === undefined)
                return Error('Parent id doesnt exists');
            entity.level = parent.level + 1;
            entity.parent = args.parent;
            parent.relatedEntities.push(entity);
            yield r.db(config.db).table(config.table).get(args.parent).update(parent);

        }

        yield r.db(config.db).table(config.table).insert(entity);
        return entity;
    }),
    updateEntity: bluebird.coroutine(function*(args, newentity) {
        if (args.id === null || args.id === undefined)
            return Error("id parameter doesn't exist");
        return yield r.db(config.db).table(config.table).get(args.id).update(newentity);
    }),
    deleteEntity: bluebird.coroutine(function*(args) {
        if (args.id === null || args.id === undefined)
            return Error("id parameter doesn't exist");
        var stack = [];
        var entity = yield r.db(config.db).table(config.table).get(args.id);
        stack.push(entity);
        while (stack.length > 0) {
            var ent = stack.pop();
            for (var i = 0; i < ent.relatedEntities.length; i++) {
                stack.push(ent.relatedEntities[i]);
            }
            yield r.db(config.db).table(config.table).get(ent.id).delete();
        }
        return entity;
    })

};