var service = require("../entityService");

service.initDb();

var resolvers = {
    Query: {
        Entities: () => {
            return service.getEntities();
        },
        Entity: (_, args, context) => {
            return service.getEntity(args);
        }
    },
    Mutation: {
        createEntity: (root, args, context) => {
            var entity = service.createEntity(args);
            return entity;
        },
        updateEntity: (root, args, context) => {
            console.log(root, args, context, "at Update Entity");
            return service.updateEntity(args);
        },
        deleteEntity: (root, args, context) => {
            return service.deleteEntity(args);
        },
    }

};

module.exports = resolvers;