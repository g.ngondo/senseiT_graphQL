var { makeExecutableSchema } = require('graphql-tools');
var fs = require('fs');
var path = require('path');

var schemaFile = path.join(__dirname, 'schema.graphql');
var typeDefs = fs.readFileSync(schemaFile, 'utf8');

var resolvers = require("./resolvers");

var schema = makeExecutableSchema({ typeDefs, resolvers });

module.exports = schema;