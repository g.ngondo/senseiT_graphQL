var config = require("./config.js");
var rethinkdbdash = require('rethinkdbdash');

var r = rethinkdbdash(config.rethinkdb);

module.exports = r;